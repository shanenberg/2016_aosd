public class Auto implements AutoIF {
	
	AutoIF _this;
	
	public AutoIF get_this() {
		return _this;
	}

	public void set_this(AutoIF _this) {
		this._this = _this;
	}

	private LenkradIF l;
	private Motor m;

	public Motor getM() {
		return m;
	}

	@Override
	public void setM(Motor m) {
		this.m = m;
	}

	/* (non-Javadoc)
	 * @see AutoIF#getL()
	 */
	@Override
	public LenkradIF getL() {
		return l;
	}

	/* (non-Javadoc)
	 * @see AutoIF#setL(Lenkrad)
	 */
	@Override
	public void setL(LenkradIF l) {
		this.l = l;
	}
	
	@Override
	public void doSet() {
		_this.setL(new Lenkrad());
		_this.setM(new Motor());
	}
	
	// Auslagern in Setter 
	// 
}
