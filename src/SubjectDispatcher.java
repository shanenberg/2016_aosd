import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

public class SubjectDispatcher<SubjectTYPE> 
			implements InvocationHandler, Subject {

	SubjectTYPE subject;
	ArrayList<Observer<SubjectTYPE>> observers = new ArrayList<Observer<SubjectTYPE>>();
	
	public SubjectDispatcher(SubjectTYPE object) {
		super();
		this.subject = object;
	}

	@Override
	public Object invoke(Object arg0, Method m, Object[] args)
			throws Throwable {
		
		if (m.getDeclaringClass()==Subject.class) {
			return m.invoke(this, args);
		}
		
		Object ret= m.invoke(subject, args);

		if(m.getName().startsWith("set")) {
			this.notifyObservers();
		}
		
		return ret;


	}

	private void notifyObservers() {
		for(Observer<SubjectTYPE> observer: observers) {
			observer.update(subject);
		}
		
	}

	@Override
	public void attach(Observer<?> obj) {
		observers.add((Observer) obj);
	}

	@Override
	public void detach(Observer<?> obj) {
		observers.remove(obj);
	}

}
