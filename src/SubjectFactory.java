import java.lang.reflect.Proxy;


public class SubjectFactory<SubjectType> {
	
	public SubjectType createSubject(SubjectType s, Class<SubjectType> clazz) {
		
		SubjectDispatcher<SubjectType> sd = 
				new SubjectDispatcher<SubjectType>(s);
		
		SubjectType proxy = (SubjectType) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(), 
				new Class[] {clazz, Subject.class}, sd);		
		return proxy;
	}
}
